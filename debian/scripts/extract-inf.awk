#!/bin/awk -f

BEGIN {
    # VAR 'ns': namespace prefix for generated shell variables

    print "typeset -a " ns "DEVICES";
}

# Strip DOS EOF
{ sub(//, ""); }

# Skip empty or comment-only lines
/^\s*(;.*)?$/ {
    next;
}

# Parse section name:
/^\s*\[/ {
    # Example: [A.b.C.d]
    if (0 == match($0, /\[([^\]]+)/, m)) {
        print "# Failed to parse section: " $0;
        exit 1;
    }
    currentSection = m[1];
    next;
}

# Parse attribute assignments
/^\s*\S+\s*=/ {
    # eat duplicated whitspace characters
    gsub(/\s+/, " ");

    # Example: ServiceType = 1 ;SERVICE_KERNEL_DRIVER
    if (0 == match($0, /^(\S+)\s?=\s?(\S|(\S.*\S))\s?$/, m)) {
        print "# Failed to parse key-pair: " $0;
        exit 1;
    }

    # bash variable name
    _key = key = currentSection "." m[1];
    gsub(/\.|%/, "_", _key);

    if (_key == "Version_DriverVer") {
        # Example: DriverVer=03/29/2017,12.0.1.1012
        if (0 == match(m[2], /(.*),(.*)/, n)) {
            print "# Failed to parse key-pair: " $0;
            exit 1;
        }

        print ns "Version_DriverDate=" n[1];
        print ns "Version_DriverVer=" n[2];
    } else if (match(m[1], /^%.*.DeviceDesc%/)) {
        # Example: %BRCMBt40USB.DeviceDesc%=RAMUSBE066, USB\VID_105B&Pid_E066 ; LenovoChina 43228+20702 combo
        if (0 == match(m[2], /([^,]+),\s?USB\\[Vv][Ii][Dd]_([0-9A-Fa-f]+)&[Pp][Ii][Dd]_([0-9A-Fa-f]+)\s?;\s?(.*)/, d)) {
            print "# Failed to parse device description: " $0;
            exit 1;
        }

        if (d[1] in devices) {
            print "# Skipping duplicated device: " $0;
        } else {
            devices[d[1]] = 1;
            print ns "DEVICES+=( \"" d[1] "\" )";
            print "  " ns "" d[1] "_NAME=\"" d[4] "\"";
            print "  " ns "" d[1] "_USBVID=\"" tolower(d[2]) "\"";
            print "  " ns "" d[1] "_USBPID=\"" tolower(d[3]) "\"";
            print "  declare -a " ns "" d[1] "_COPYLIST";
        }
    }
    next;
}

{
    # Example: [RAMUSB21E8.CopyList]
    if (match(currentSection, /^(.*)\.CopyList$/, m)) {
        if (m[1] in devices) {
            print "  " ns "" m[1] "_COPYLIST+=( \"" $0 "\" )";
        }
    }
}
